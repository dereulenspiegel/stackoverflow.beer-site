---
title: MAIne Güte
description: 'Slightly darker, reddish Mai Bock. It''s malty, it''s strong, it''s
    bitter. Perfect to digest all the apocalyptic news this year. '
abv: 6.8
ibu: 34
og: 15.439753352859059
fg: 0
buGuRation: 0.54
beerColor: 10.5
untappdId: "4810249"
malts:
  - name: Caramel Hell
    supplier: BestMalz
    amount: 0.76
  - name: Melanoidin
    supplier: BestMalz
    amount: 0.18
  - name: Pilsen Malt
    supplier: BestMalz
    amount: 3.98
  - name: Vienna
    supplier: BestMalz
    amount: 2.6
  - name: Carared
    supplier: Weyermann
    amount: 0.23
hops:
  - name: Cascade
    origin: Germany
    amount: 22
    usage: Both
    alpha: 6
  - name: Perle
    origin: Germany
    amount: 15
    usage: Aroma
    alpha: 5
  - name: Saphir
    origin: Germany
    amount: 65
    usage: Aroma
    alpha: 3
miscs: []
yeasts:
  - name: W34 / 70
    laboratory: Gozdawa
    productId: W3470
lastBrewDate: "2022-03-05T23:00:00Z"
lastBatch: 26
author: StackOverflow Brewery
date: 2022-03-05T23:00:00Z
tags:
  - Bier
categories:
  - Bier
comments: false
removeBlur: false
draft: false

---
