---
title: Oida!
description: 'Malty, toasty and high drinkability. The perfect combination in winter
    for cozy evenings '
abv: 5.3
ibu: 30
og: 12.861598442176028
fg: 0
buGuRation: 0.57
beerColor: 15.5
untappdId: "4681946"
malts:
  - name: Caramel Munich I
    supplier: BestMalz
    amount: 0.66
  - name: Melanoidin
    supplier: BestMalz
    amount: 0.12
  - name: Pilsen Malt
    supplier: BestMalz
    amount: 0.26
  - name: Vienna
    supplier: BestMalz
    amount: 4.21
  - name: Carafa I
    supplier: Weyermann
    amount: 0.11
hops:
  - name: Hallertauer Mittelfrueh
    origin: Germany
    amount: 19.4996
    usage: Aroma
    alpha: 4
  - name: Magnum
    origin: US
    amount: 17.7
    usage: Bittering
    alpha: 14
miscs: []
yeasts:
  - name: Munich Lager
    laboratory: Wyeast Labs
    productId: "2308"
lastBrewDate: "2021-12-18T23:00:00Z"
lastBatch: 22
author: StackOverflow Brewery
date: 2021-12-18T23:00:00Z
tags:
  - Bier
categories:
  - Bier
comments: false
removeBlur: false
draft: false

---
