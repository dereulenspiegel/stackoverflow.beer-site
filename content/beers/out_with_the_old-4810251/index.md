---
title: Out With the Old
description: 'Classic Altbier from Dortmunder inspired by the Düsseldorfer original.
    The whirlpool hops add a new slightly fruity characteristic without overpowering
    the classical character. '
abv: 5.1
ibu: 36
og: 11.912080756223979
fg: 0
buGuRation: 0.76
beerColor: 16.3
untappdId: "4810251"
malts:
  - name: Caramel Munich II
    supplier: BestMalz
    amount: 0.03
  - name: Chocolate
    supplier: BestMalz
    amount: 0.048
  - name: Smoked
    supplier: BestMalz
    amount: 0.042
  - name: Vienna
    supplier: BestMalz
    amount: 0.364
  - name: BlackSwaen Honey Biscuit
    supplier: The Swaen
    amount: 0.035
  - name: Floor-Malted Bohemian Pilsner
    supplier: Weyermann
    amount: 1.076
hops:
  - name: Hallertauer Mittelfrueh
    origin: Germany
    amount: 6.8
    usage: Aroma
    alpha: 5.2
  - name: Spalter Select
    origin: Germany
    amount: 28.7
    usage: Aroma
    alpha: 4.75
miscs: []
yeasts:
  - name: Dusseldorf Alt Yeast
    laboratory: White Labs
    productId: WLP036
lastBrewDate: "2022-03-24T23:00:00Z"
lastBatch: 27
author: StackOverflow Brewery
date: 2022-03-24T23:00:00Z
tags:
  - Bier
categories:
  - Bier
comments: false
removeBlur: false
draft: false

---
